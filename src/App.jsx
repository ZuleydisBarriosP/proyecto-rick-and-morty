import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import { Routes, Route } from 'react-router-dom';
import { Footer } from './components/Footer/Footer';
import { Header } from './components/Navbar/Header';
import { Home } from './pages/Home';
import { Personaje } from './pages/Personaje';

function App() {
  return (
    <div className='App'>
      {/* Todolo que este aqui se ve en todas las paginas */}
      <Header />

      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/personaje/:personajeId' element={<Personaje />} />
      </Routes>
      <Footer />
      {/* Todolo que este aqui se ve en todas las paginas */}
    </div>
  );
}

export default App;
