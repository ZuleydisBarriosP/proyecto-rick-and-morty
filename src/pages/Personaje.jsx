import { useParams } from 'react-router-dom';
import { useState, useEffect } from 'react';
import axios from 'axios';
import BannerPersonaje from '../components/Personajes/BannerPersonaje';
import { EpisodioListado } from '../components/Episodios/EspisodioListado';

export function Personaje() {
  let { personajeId } = useParams();
  let [personaje, setPersonaje] = useState(null);
  let [episodio, setEpisodios] = useState(null);

  useEffect(() => {
    axios
      .get(`https://rickandmortyapi.com/api/character/${personajeId}`)
      .then((respuesta) => {
        setPersonaje(respuesta.data);
        console.log(respuesta.data);
        let { episode } = respuesta.data;
        let peticiones = episode.map((URLEpisodio) => axios.get(URLEpisodio));
        Promise.all(peticiones).then((respuesta) => {
          let datos = respuesta.map((episodio) => episodio.data);
          setEpisodios(datos);
        });
      });
  }, []);

  return (
    <div className='p-5'>
      {personaje ? (
        <div>
          <BannerPersonaje {...personaje} />
          <h2 className='py-4'>Episodios</h2>
          {episodio ? (
            <EpisodioListado datos={episodio} />
          ) : (
            <h4>Cargando Episodios...</h4>
          )}
        </div>
      ) : (
        <div>Cargando Personajes...</div>
      )}
    </div>
  );
}
