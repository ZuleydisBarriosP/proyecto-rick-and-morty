import { EpisodioItem } from './EpisodioItem';

export function EpisodioListado({ datos }) {
  return (
    <div className='row'>
      {datos.map((episode) => {
        return <EpisodioItem {...episode} key={episode.id} />;
      })}
    </div>
  );
}
