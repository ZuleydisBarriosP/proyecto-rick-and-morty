import { Link } from 'react-router-dom';

export function Header() {
  return (
    <nav className='navbar  navbar-expand-lg navbar-warning bg-warning'>
      <div className='container-fluid'>
        <Link className='text-black nav-link fs-3' to='/'>
          Rick and Morty
        </Link>
        <div className='collapse navbar-collapse' id='navbarText'>
          <ul className='navbar-nav me-auto mb-2 mb-lg-0'>
            <li className='nav-item'>
              <Link className='text-black nav-link fs-3' to='/'>
                Home
              </Link>
            </li>
          </ul>
          {/*  <Link className='text-black nav-link' to='/personaje'>
            <span className='navbar-text'>Personajes</span>
          </Link> */}
        </div>
      </div>
    </nav>
  );
}
