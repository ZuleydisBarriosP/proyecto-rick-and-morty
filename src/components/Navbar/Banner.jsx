import bannerImg from '../../assets/img/banner-img.jpg';
export function Banner() {
  return (
    <div>
      <img
        style={{ height: '100%', objectFit: 'cover' }}
        src={bannerImg}
        className='card-img'
        alt='Banner-RickandMorty'
      />
    </div>
  );
}
