import { useEffect, useState } from 'react';

import { PersonajeItem } from './PersonajeItem';
import axios from 'axios';

export function ListadoPersonajes({ buscar }) {
  const [personajes, setPersonajes] = useState(null);
  let filtroPersonajes = personajes?.results;

  if (buscar && personajes)
    filtroPersonajes = personajes.results.filter((personaje) =>
      personaje.name.toLowerCase().includes(buscar.toLowerCase())
    );

  useEffect(() => {
    // axios permite hacer peticiones a otros servicios de una manera mas sencilla de fetch
    axios.get('https://rickandmortyapi.com/api/character').then((respuesta) => {
      setPersonajes(respuesta.data);
    });
  }, []);

  return (
    <div>
      <div className='lista row py-1'>
        {filtroPersonajes ? (
          filtroPersonajes.map((personaje) => {
            return <PersonajeItem key={personaje.id} {...personaje} />;
          })
        ) : (
          <h3>Cargando...</h3>
        )}
      </div>
    </div>
  );
}
