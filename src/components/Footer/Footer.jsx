export function Footer() {
  return (
    <nav className='navbar-warning bg-warning'>
      <div className='container-fluid'>
        <h6 className='text-center py-3 mb-0 fs-3'>
          Proyecto creado por Zuleydis Barrios Peña - CARIV <br />
          2022{' '}
        </h6>
      </div>
    </nav>
  );
}
