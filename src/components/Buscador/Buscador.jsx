export function Buscador({ valor, onBuscador }) {
  return (
    <div className='d-flex justify-content-end'>
      <div className='mb-3 col-5'>
        <input
          type='text'
          className='form-control'
          placeholder='Buscar personajes...'
          value={valor}
          onChange={(e) => onBuscador(e.target.value)}
        />
      </div>
    </div>
  );
}
