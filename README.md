# __PROYECTO RICK AND MORTY__

# Pasos para correr el proyecto:

- Descomprimir el archivo .zip
- Entrar a la carpeta y abrir la terminal
- Ejecutar npm install  
- Correr el proyecto con npm run dev
- Abrir en el http://localhost:3000


# *Tecnologias usadas:*

### React
### Javascript
### Bootstrap
### React-Router-DOM
### Gitlab
### Backlog de actividades

## Pasos para su elaboracion
1. Crear el proyecto usando vite y crear el repositorio en gitlab
2. Configurar prettier para formatear nuestro codigo y que se vea organizado
3. Instalar react-router-dom, axios, bootstrap
4. Crear componente de cabecera
5. Crear componente banner
6. Crear componente de busqueda de personajes
7. Crear componente de personajes
8. Crear componente de listado de personajes
9. Crear componente de footer (Pie de pagina)
10. Pagina de detalle de episodio
11. Crear componente de detalle del personaje
12. Crear componente de episodios
13. Crear componente de listado de episodios
14. Integracion de servicios
15. Integrar servicio de listado de episodios
16. Integrar servicio de detalle de personaje
17. Integrar servicio de episodio de personajes


12 - Febrero - 2022
